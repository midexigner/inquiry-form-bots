<?php
/*
/*
Plugin Name:  Inquiry Form Bots
Plugin URI:
Description: Simple Inquiry Form Bots
Version:      1.0
Author:
Author URI:   https://developer.wordpress.org/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  flk
*/


defined('ABSPATH') or die('Hey, you can\t access this file, you silly human');

require 'include/enqueue.php';
require 'include/ajax.php';
require 'include/post-type.php';
// Inquiry Form shortcode
function mi_support_form($atts,$content = null){
 // [inquiry_form_bots]

 // get the attributes
 $atts = shortcode_atts(
   array(),
   $atts,
   'inquiry_form_bots'
 );
 // return HTML
 #return 'This is the contact form generated HTML';
 ob_start();
 include 'include/supports-form.php';
 return ob_get_clean();
}
add_shortcode( 'inquiry_form_bots', 'mi_support_form' );

function mailtrap($phpmailer) {
  $phpmailer->isSMTP();
  $phpmailer->Host = 'smtp.mailtrap.io';
  $phpmailer->SMTPAuth = true;
  $phpmailer->Port = 25;
  $phpmailer->Username = 'cdd842d905283c';
  $phpmailer->Password = '942f9aa5eb8811';
}

add_action('phpmailer_init', 'mailtrap');

// Activation Code
/*
function pluginprefix_setup_post_type() {
    // register the "book" custom post type
    register_post_type( 'book', ['public' => 'true'] );
}
add_action( 'init', 'pluginprefix_setup_post_type' );
*/
function pluginprefix_install() {
    // trigger our function that registers the custom post type
    rmcc_create_post_type();
    // clear the permalinks after the post type has been registered
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'pluginprefix_install' );

// deactivation Code

function pluginprefix_deactivation() {
    // unregister the post type, so the rules are no longer in memory
    unregister_post_type( 'cform_inquiry' );
    // clear the permalinks to remove our post type's rules from the database
    flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'pluginprefix_deactivation' );

?>
