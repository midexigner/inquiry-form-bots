<?php
/*
@package mitheme
    ========================
        AJAX FUNCTIONS
    ========================
*/
add_action('wp_ajax_nopriv_flk_save_user_form_one', 'flk_save_contact_one');
add_action('wp_ajax_flk_save_user_form_one', 'flk_save_contact_one');
function flk_save_contact_one()
{
    $fullname = wp_strip_all_tags($_POST['fullname']);
    $emailaddress = wp_strip_all_tags($_POST['emailaddress']);
    $phonenumber = wp_strip_all_tags($_POST['phonenumber']);
    $longwant = wp_strip_all_tags($_POST['longwant']);
    $somethingabout = wp_strip_all_tags($_POST['somethingabout']);

    echo "fullname" . $fullname . '<br/>';
    echo "emailaddress" . $emailaddress . '<br/>';
    echo "phonenumber" . $phonenumber . '<br/>';
    echo "longwant" . $longwant . '<br/>';
    echo "somethingabout" . $somethingabout . '<br/>';
   $args = array(
        'post_title' => $fullname,
       // 'post_content' => $message,
        'post_author' => 1,
        'post_status' => 'publish',
        'post_type' => 'cform_inquiry',
        'meta_input' => array(
            'fullName' => $fullName,
        ),
    );
   $postID = wp_insert_post($args);
       if ($postID !== 0) {
       $message = $fullname.'<br/>'.$emailaddress.'<br/>'.$phonenumber.'<br/>'.$longwant.'<br/>'.$somethingabout;
        $to = get_bloginfo('admin_email');
        $subject = 'Support Form - '.$fullname;
        $headers[] = 'From: '.get_bloginfo('name').' <'.$to.'>'; 
        $headers[] = 'Reply-To: '.$fullname.' <'.$emailaddress.'>';
        $headers[] = 'Content-Type: text/html: charset=UTF-8';
        wp_mail($to, $subject, $message, $headers);
    }
    echo $postID;
    die();
}