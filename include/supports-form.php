<section id="support-form">

    <form action="#" class="form-hor" role="form" id="flkSupportForm" method="post" data-url='<?php echo admin_url('admin-ajax.php'); ?>' autocomplete='off'>
      <div class="row">

   
        <div class="lead typeOne" data-text="Hello, my name is Michael and I am robot! How may I help you today?">
        </div>
     <div class="group-form" id="choose">
          <!--  -->
        <div class="radio">
        <label class="typing">
        <input type="radio" name="choose" value="iRentAppartment"> 
       I want to rent an apartment
      </label>
    </div>
    <div class="radio">
      <label  class="typing">
        <input type="radio" name="choose" value="iGeneralQuestion"> 
      I have a general question to your service
      </label>
    </div>
      </div>
    </div>

<div class="row" id="rent-apartment">

<p class="lead typeTwo"></p>
        <div class="group-form" id="fullName">
       <label for="fullname">What is your full name?</label>
       <input type="text" class="control-form" name="fullname" id="fullname">
      </div>
      <div id="msgOne"><p class="lead typedFive  success-text"></p></div>
      <div class="group-form" id="emailAddress">
       <label for="emailaddress">What is your email address?</label>
       <input type="text" class="control-form" name="emailaddress" id="emailaddress" autofocus>
      </div>
      <div class="group-form" id="phoneNumber">
        <label for="phonenumber">What is your phone number?</label>
       <input type="text" class="control-form" name="phonenumber" id="phonenumber" autofocus>
      </div>

     <!--  <div class="group-form" id="objectIdappartments">
       <label for="objectidappartment">What is the name or object ID of the apartment?</label>
     <label class="typing">
       <input type="radio" name="objectidappartments" value="idonotknow" autofocus> 
     I don´t know
     </label>
     <label  class="typing">
       <input type="radio" name="objectidappartments" value="iknow" autofocus> 
          I know
     </label>
     
     
     <div id="idonotknowText">
     <p class='typedSix lead text-success'></p>  
     </div>
      <input type="text" class="control-form" name="objectidappartmentOther" id="objectidappartmentOther" placeholder="Other" autofocus>
     </div> -->

       <div class="group-form" id="longWants">
        <label for="longwant">How long do you want to stay?</label>
       <input type="text" class="control-form" name="longwant" id="longwant" autofocus>
      </div>

       <div class="group-form" id="somethingAbouts">
        <label for="somethingabout">Do you want us to know something about you?</label>
       <textarea name="somethingabout"  class="control-form" id="somethingabout" cols="30" rows="10" autofocus></textarea>
      </div>
  
</div>

<!-- //option 2 -->
 <div class="row" id="general-question">
        <div class="typeThree lead"> </div>
<div class="group-form" id="faqSection">
       <label class="typing">
        <input type="radio" name="faqsection" value="yes"> 
      Yes
      </label>
      <label class="typing">
        <input type="radio" name="faqsection" value="no"> 
      NO
      </label>
      </div>
      <div class="alert" id="questionNo">
      <p class="typeEight lead"></p>
   <div id="ThanksTwo"> <a href="<?php echo site_url(); ?>/#section6" class="button" target="_blank">More info</a></div>
      </div>
      <div id="questionYes">
     <p class="typeFour lead"></p>
    
    </div> 

<div class="group-form" id="StaisfySection">
  <p class="lead typedSeven">Are You Satisfy with the result?</p>
       <label class="typing">
        <input type="radio" name="staisfysection" value="yes"> 
      Yes
      </label>
      <label class="typing">
        <input type="radio" name="staisfysection" value="no"> 
      NO
      </label>
</div>

   <div class="group-form" id="fullNameOne">
       <label for="fullname">What is your full name?</label>
       <input type="text" class="control-form" name="fullname" id="fullnameOne">
      </div>

      <div class="group-form" id="emailAddressOne">
         <label for="emailaddress">What is your email address?</label>
       <input type="email" class="control-form" name="emailaddressOne" id="emailaddressOne">
      </div>
      <div class="group-form" id="phoneNumberOne">
        <label for="phonenumber">What is your phone number?</label>
       <input type="text" class="control-form" name="phonenumber" id="phonenumberOne">
      </div>
<div class="group-form" id="helpYou">
        <label for="helpyou">How can we help you with?</label>
       <input type="text" class="control-form" name="helpyou" id="helpyou">
      </div>


  </div>
  <div id="ThankYouYes">
       <p class="typeSeven lead"></p>
      </div>

           <!-- <button type="submit" class="button  submit">Send</button>-->
<small class="text-info form-control-msg js-form-submission">Submission in process, please wait..</small>
  <small class="text-success form-control-msg js-form-success">Message Successfully submitted, thank you!</small>
<small class="text-danger form-control-msg js-form-error">There was a problem with the Contact Form, please try again!</small>
        </form>

</section>
