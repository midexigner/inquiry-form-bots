<?php



/*

	========================
		FRONT-END ENQUEUE FUNCTIONS
	========================
*/


function mi_plugin_scripts(){
	wp_enqueue_style( 'style', plugin_dir_url(__FILE__). '../assets/css/style.css', array(), '1.0.0', 'all' );
	wp_register_script( 'typed' , plugin_dir_url(__FILE__). '../assets/js/typed.js', false, '1.0.0', false );
	wp_register_script( 'slimscroll' ,'https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js', false, '1.0.0', false );
	wp_register_script( 'plugins' , plugin_dir_url(__FILE__). '../assets/js/scripts.js', false, '1.0.0', false );
	wp_enqueue_script( 'slimscroll' );
	wp_enqueue_script( 'typed' );
	wp_enqueue_script( 'plugins' );
}
add_action( 'wp_enqueue_scripts', 'mi_plugin_scripts' );






 ?>
