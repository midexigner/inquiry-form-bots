<?php 

add_action( 'init', 'rmcc_create_post_type' );
function rmcc_create_post_type() {  // clothes custom post type
    // set up labels
    $labels = array(
        'name' => 'Contact Inquiry',
        'singular_name' => 'Contact Inquiry Item',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Contact Inquiry Item',
        'edit_item' => 'Edit Contact Inquiry Item',
        'new_item' => 'New Contact Inquiry Item',
        'all_items' => 'All Contact Inquiry',
        'view_item' => 'View Contact Inquiry Item',
        'search_items' => 'Search Contact Inquiry',
        'not_found' =>  'No Contact Inquiry Found',
        'not_found_in_trash' => 'No Contact Inquiry found in Trash',
        'menu_icon'=> 'dashicons-editor-help',
        'parent_item_colon' => '',
        'menu_name' => 'Contact Form Inquiry',
    );
    register_post_type(
        'cform_inquiry',
        array(
            'labels' => $labels,
            'has_archive' => true,
            'public' => true,
            'hierarchical' => true,
            'supports' => array( 'title'),
            //'taxonomies' => array( 'post_tag', 'category' ),
            'exclude_from_search' => true,
            'capability_type' => 'post',
            'capabilities' => array(
    //'create_posts' => 'do_not_allow', // false < WP 4.5, credit @Ewout
  ),
  //'map_meta_cap' => true, // Set to `false`, if users are not allowed to edit/delete existing posts
        )
    );
}
 
/*-----------------------------------------------------------------------------------*/
/*  Add Metabox to Slider
/*-----------------------------------------------------------------------------------*/
    add_action( 'add_meta_boxes', 'slide_meta_box_add' );

    function slide_meta_box_add()
    {
        add_meta_box( 'slide-meta-box', __('Slider Options', 'bots'), 'slide_meta_box', 'cform_inquiry', 'normal', 'high' );
    }

    function slide_meta_box( $post )
    {
        $values = get_post_custom( $post->ID );
        $fullName = isset( $values['fullName'] ) ? esc_attr( $values['fullName'][0] ) : '';
        $emailAddress = isset( $values['emailAddress'] ) ? esc_attr( $values['emailAddress'][0] ) : '';
        $phoneNumber = isset( $values['phoneNumber'] ) ? esc_attr( $values['phoneNumber'][0] ) : '';
        $longWants = isset( $values['longWants'] ) ? esc_attr( $values['longWants'][0] ) : '';
        $somethingAbout = isset( $values['somethingAbout'] ) ? esc_attr( $values['somethingAbout'][0] ) : '';
         $helpyou = isset( $values['helpyou'] ) ? esc_attr( $values['helpyou'][0] ) : '';
        wp_nonce_field( 'slide_meta_box_nonce', 'meta_box_nonce_slide' );
        ?>
<table style="width:100%;" class="form-table">
     <tr>
      <th style="width:25%"><label for="fullName"><strong><?php _e('Name','bots');?></strong>
        </label></th>
    <td>
    <input type="text" name="fullName" id="fullName" style="width:70%; margin-right:4%;" value="<?php echo trim($fullName); ?>">

      </td>
  </tr>
    <tr>
      <th style="width:25%"><label for="emailAddress"><strong><?php _e('Email Address','bots');?></strong></label>
            </th>
    <td> <input type="text" name="emailAddress" id="emailAddress" style="width:70%; margin-right:4%;" value="<?php echo trim($emailAddress); ?>">
      </td>
  </tr>
<tr>
      <th style="width:25%"><label for="phoneNumber"><strong><?php _e('Phone Number','bots');?></strong></label>
            </th>
    <td> <input type="text" name="phoneNumber" id="phoneNumber" style="width:70%; margin-right:4%;" value="<?php echo trim($phoneNumber); ?>">
      </td>
  </tr>
  <tr>
       <th style="width:25%"><label for="longWants"><strong><?php _e('How long do you want to stay?','bots');?></strong>
                 </label></th>
    <td><input type="text" name="longWants" id="longWants" value="<?php echo $longWants; ?>" style="width:70%; margin-right:4%;" />
     </td>
  </tr>
    <tr>
             <th style="width:25%"><label for="somethingAbout"><strong><?php _e('Do you want us to know something about you?','bots');?></strong>
                 </label></th>
        <td><textarea type="text" name="somethingAbout" id="somethingAbout" style="width:70%; margin-right:4%;" ><?php echo $somethingAbout; ?></textarea>
         </td>
    </tr>
</table>
<?php
    }
add_action( 'save_post', 'slide_meta_box_save' );

    function slide_meta_box_save( $post_id )
    {

    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

        if( !isset( $_POST['meta_box_nonce_slide'] ) || !wp_verify_nonce( $_POST['meta_box_nonce_slide'], 'slide_meta_box_nonce' ) ) return;

        if( !current_user_can( 'edit_post' ) ) return;

            if( isset( $_POST['caption'] ) )
            update_post_meta( $post_id, 'caption', $_POST['caption'] );
        if( isset( $_POST['sub_title'] ) )
            update_post_meta( $post_id, 'sub_title', $_POST['sub_title']  );
            if( isset( $_POST['url'] ) )
                update_post_meta( $post_id, 'url', $_POST['url']  );
                if( isset( $_POST['button_text'] ) )
                    update_post_meta( $post_id, 'button_text', $_POST['button_text']  );
        if( isset( $_POST['styling'] ) )
            update_post_meta( $post_id, 'styling', $_POST['styling']  );

}


?>